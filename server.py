import os
import json
import requests
import pickle
import numpy as np
import json
from PIL import Image, ImageFont, ImageDraw, ImageEnhance


from flask import Flask, render_template, request, jsonify, redirect

app = Flask(__name__, static_folder='static', static_url_path='/static')

@app.route('/')
def welcome_page():
	return render_template('index.htm')

@app.route("/upload_image", methods=["GET"])
def upload_image():
	return render_template("upload_image.htm")

@app.route("/process_upload", methods=["POST"])
def process_upload():
	if request.method == "POST":
		if request.files:
			image = request.files["image"]

			print(image)
			print(request.url)

			image.save(os.path.join('uploads',image.filename))

			threshold = float(request.form['threshold'])
			
			params = (
				('version', '2019-02-11'),
			)

			files = {
				'threshold': (None, str(threshold) ),
				'features': (None, 'objects'),
				'collection_ids': (None, '910f009e-6d58-4a85-bfc9-3c7093a64e1a'),
				'images_file': (image.filename, open(os.path.join('uploads',image.filename), 'rb')),
			}

			visual_credentials = {}
			
			try:
				with open('visual_credentials.json') as creds:
					visual_credentials = json.load(creds)
			except:
				print("No Credentials Found")

			response = requests.post('https://gateway.watsonplatform.net/visual-recognition/api/v4/analyze', params=params, files=files, auth=('apikey', visual_credentials['apikey']))
			jsponse = response.json()


			if 'collections' in jsponse['images'][0]['objects']:
				print(jsponse['images'][0]['objects']['collections'][0]['objects'])

				source_image = Image.open(os.path.join('uploads',image.filename)).convert("RGBA")
				draw = ImageDraw.Draw(source_image)

				for found_object in jsponse['images'][0]['objects']['collections'][0]['objects']:
					x0 = found_object["location"]["left"]
					y0 = found_object["location"]["top"]
					x1 = x0 + found_object["location"]["width"]
					y1 = y0 + found_object["location"]["height"]

					draw.rectangle(((x0,y0),(x1,y1)), outline='red')
					draw.text((x0, y0), "{object} - {score:.2f}".format(object=found_object['object'], score=found_object['score']), fill='red')

				source_image.save('static/' + image.filename + '.png', "PNG")

				return redirect('static/' + image.filename + '.png')


			return "No Bridges Found"

port = os.getenv('VCAP_APP_PORT', '5000')

if __name__ == "__main__":
	app.run(host='0.0.0.0', port=int(port))
